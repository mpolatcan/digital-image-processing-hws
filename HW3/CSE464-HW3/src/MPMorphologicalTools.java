import vpt.ByteImage;
import vpt.Image;
import vpt.algorithms.point.Inversion;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Stack;

/**
 * Created by mpolatcan-gyte_cse on 16.04.2017.
 */
public class MPMorphologicalTools {
    public static int EROSION = 0;
    public static int MAX = 0;
    public static int DILATION = 1;
    public static int MIN = 1;

    public static int[][] largeMask = {{1,1,1,1,1,1,1,1,1,1,1},
                                       {1,1,1,1,1,1,1,1,1,1,1},
                                       {1,1,1,1,1,1,1,1,1,1,1},
                                       {1,1,1,1,1,1,1,1,1,1,1},
                                       {1,1,1,1,1,1,1,1,1,1,1},
                                       {1,1,1,1,1,0,1,1,1,1,1},
                                       {1,1,1,1,1,1,1,1,1,1,1},
                                       {1,1,1,1,1,1,1,1,1,1,1},
                                       {1,1,1,1,1,1,1,1,1,1,1},
                                       {1,1,1,1,1,1,1,1,1,1,1},
                                       {1,1,1,1,1,1,1,1,1,1,1}};
    public static int[][] xlargeMask =
            {{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}};
    public static int[][] plusMask = {{1,1,1,1,1},
                                      {1,1,1,1,1},
                                      {1,1,0,1,1},
                                      {1,1,1,1,1},
                                      {1,1,1,1,1}};

    public static int[][] squareSE(int size) {
        int[][] squareSE = new int[size][size];

        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; j++) {
                if ((i == (size / 2)) && (j == (size / 2))) {
                    squareSE[i][j] = 0;
                } else {
                    squareSE[i][j] = 1;
                }
                System.out.print(squareSE[i][j] + " ");
            }
            System.out.println();
        }

        return squareSE;
    }

    public static Image applyMorphologicalOperator(Image img, int[][] mask, int operation) {
        Image result = new ByteImage(img.getXDim(),img.getYDim());

        int maskSize = (mask.length - 1) / 2;

        for (int i = 0; i < img.getXDim(); i++) {
            for (int j = 0; j < img.getYDim(); j++) {
                ArrayList<Integer> intensities = new ArrayList<>();

                for (int k = 0; k < mask.length; ++k) {
                    for (int l = 0; l < mask.length; ++l) {
                        if (mask[k][l] == 1 && checkBoundaries(img, i + l - maskSize, j + k - maskSize)) {
                            intensities.add(img.getXYByte(i + l - maskSize, j + k - maskSize));
                        }
                    }
                }

                intensities.sort(Comparator.naturalOrder());

                if (operation == EROSION) {
                    result.setXYByte(i, j, intensities.get(0));
                } else if (operation == DILATION) {
                    result.setXYByte(i, j, intensities.get(intensities.size() - 1));
                }
            }
        }

        return result;
    }

    public static Image morphologicalReconstruction(Image img, int[][] mask, Image imgMask, int operation) {
        Stack<Image> prevImages = new Stack<>();
        Image reconstructedImage;

        reconstructedImage = getMinOrMax(applyMorphologicalOperator(img,mask,operation),
                imgMask,operation);
        prevImages.add(reconstructedImage.newInstance(true));

        while (true) {
            reconstructedImage = getMinOrMax(applyMorphologicalOperator(reconstructedImage,mask,operation),
                    imgMask,operation);

//            Display2D.invoke(reconstructedImage);
            if (isImageSame(reconstructedImage,prevImages.peek())) {
                break;
            }

            prevImages.add(reconstructedImage.newInstance(true));
        }

        return reconstructedImage;
    }


    public static Image openingByReconstruction(Image img, int[][] mask, Image imgMask) {
        return morphologicalReconstruction(applyMorphologicalOperator(img,mask,EROSION),mask,imgMask,DILATION);
    }

    public static Image closingByReconstruction(Image img, int[][] mask, Image imgMask) {
        return morphologicalReconstruction(applyMorphologicalOperator(img,mask,DILATION),mask,imgMask,DILATION);
    }

    public static Image holeFilling(Image img ,int[][] mask) {
        return morphologicalReconstruction(Inversion.invoke(img),mask,img,EROSION);
    }

    public static Image getDifference(Image firstImage, Image secondImage) {
        Image difference = new ByteImage    (firstImage.getXDim(),firstImage.getYDim());

        for (int i = 0; i < firstImage.getXDim(); i++) {
            for (int j = 0; j < firstImage.getYDim(); j++) {
                difference.setXYByte(i,j,firstImage.getXYByte(i,j)-secondImage.getXYByte(i,j));
            }
        }

        return difference;
    }

    public static Image getMinOrMax(Image dilatedImage, Image originalImage, int option) {
        Image result = new ByteImage(originalImage.getXDim(),originalImage.getYDim());

        for (int i = 0; i < originalImage.getXDim(); i++) {
            for (int j = 0; j < originalImage.getYDim(); j++) {
                if (option == MAX) {
                    result.setXYByte(i, j, Math.max(originalImage.getXYByte(i,j),
                            dilatedImage.getXYByte(i,j)));
                } else if (option == MIN) {
                    result.setXYByte(i, j, Math.min(originalImage.getXYByte(i,j),
                            dilatedImage.getXYByte(i,j)));
                }
            }
        }

        return result;
    }

    public static boolean isImageSame(Image reconstructedImg, Image prevImg) {
        boolean isSame = true;

        for (int i = 0; i < prevImg.getXDim(); i++) {
            for (int j = 0; j < prevImg.getYDim(); j++) {
                if (prevImg.getXYByte(i,j) != reconstructedImg.getXYByte(i,j)) {
                    isSame = false;
                }
            }
        }

        return isSame;
    }

    public static boolean checkBoundaries(Image img, int x, int y) {
        return (x >= 0 && x < img.getXDim()) && (y >= 0 && y < img.getYDim());
    }
}
