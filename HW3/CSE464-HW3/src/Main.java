import vpt.Image;
import vpt.algorithms.display.Display2D;
import vpt.algorithms.io.Save;
import vpt.util.Tools;

/**
 * Created by mpolatcan-gyte_cse on 24.03.2017.
 */
public class Main {
    public static void main(String[] args) {
        // I have saved Real and Imaginary images as .txt files due to
        // dip.jar doesn't support DoubleImage saving. So I have written load
        // and save functions for txt images.
        Image real = MPImageIO.loadImage("pompeii_real.txt");
        Image image = MPImageIO.loadImage("pompeii_img.txt");
        Image butterworthFilter =
                MPButterworthFilter.getButterWorthFilter(real.getXDim(),real.getYDim(),2,100,13);
        butterworthFilter = Tools.shiftOrigin(butterworthFilter);


        // Apply filter to image and real parts
        for (int i = 0; i < real.getXDim(); i++) {
            for (int j = 0; j < real.getYDim(); j++) {
                if (butterworthFilter.getXYDouble(i,j) < 0.25) {
                    real.setXYDouble(i,j,0);
                    image.setXYDouble(i,j,0);
                }
            }
        }

        Image clearedImage = MPInverseFourierTransform.execute(real,image);
        Save.invoke(clearedImage,"clearedImage.png");
        Display2D.invoke(clearedImage);
    }
}
