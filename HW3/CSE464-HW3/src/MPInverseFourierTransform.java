import vpt.ByteImage;
import vpt.DoubleImage;
import vpt.Image;
import vpt.util.Tools;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by mpolatcan-gyte_cse on 31.03.2017.
 */
public class MPInverseFourierTransform {
    public static LocalTime startTime;
    public static int taskNum;
    public static int completedTaskNum = 0;

    public static Image execute(Image fourierReal, Image fourierImage) {
        startTime = LocalTime.now();

        Image result = new DoubleImage(fourierReal.getXDim(),fourierReal.getYDim());

        startTime = LocalTime.now();

        ArrayList<MPInverseFourierTransformTask> inverseFourierTasks = new ArrayList<>();
        ExecutorService inverseFourierThreadPool = Executors.newFixedThreadPool(8);

        for (int i = 0; i < ((result.getXDim() * result.getYDim()) / 5) + 1; i++) {
            inverseFourierTasks.add(new MPInverseFourierTransformTask(result, fourierReal, fourierImage));
        }

        taskNum = inverseFourierTasks.size();

        System.out.println("Task num: " + taskNum);

        int currentTask = 0;

        for (int i = 0; i < result.getXDim(); i++) {
            for (int j = 0; j < result.getYDim(); j++) {
                if (inverseFourierTasks.get(currentTask).getPixelPackSize() == 5) {
                    ++currentTask;
                }
                inverseFourierTasks.get(currentTask).addPixelToPack(new MPPixel(i, j));
            }
        }

        for (MPInverseFourierTransformTask task : inverseFourierTasks) {
            inverseFourierThreadPool.execute(task);
        }

        inverseFourierThreadPool.shutdown();

        while (!inverseFourierThreadPool.isTerminated()) ;

        return result;
    }
}
