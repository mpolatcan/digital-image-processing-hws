import vpt.Image;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class MPInverseFourierTransformTask implements Runnable {
    private Image img,
                  fourierReal,
                  fourierImage;
    private ArrayList<MPPixel> pixelPack;

    public MPInverseFourierTransformTask(Image img,
                                         Image fourierReal,
                                         Image fourierImage) {
        pixelPack = new ArrayList<>();
        this.img = img;
        this.fourierReal = fourierReal;
        this.fourierImage = fourierImage;
    }

    @Override
    public void run() {
        int xDim = img.getXDim();
        int yDim = img.getYDim();

        double imageSizeSqrt = Math.sqrt(xDim * yDim);

        for (MPPixel pixel: pixelPack) {
            double complexReal = 0.0;
            double complexImag = 0.0;

            int u = 0;
            int v = 0;

            while (u < xDim) {
                double res = ((double)u * (double)pixel.getX() / (double)xDim +
                        (double)v * (double)pixel.getY() / (double)yDim) * (Math.PI * 2);
                double realPixelVal = fourierReal.getXYDouble(u,v);
                double imagPixelVal = fourierImage.getXYDouble(u,v);
                double resCos = Math.cos(res);
                double resSin = Math.sin(res);
                complexReal += realPixelVal * resCos - imagPixelVal * resSin;
                complexImag += imagPixelVal * resCos + realPixelVal * resSin;

                ++v;

                if (v >= yDim) {
                    v = 0;
                    ++u;
                }
            }

            complexReal = complexReal / imageSizeSqrt;
            complexImag = complexImag / imageSizeSqrt;

            img.setXYDouble(pixel.getX(),pixel.getY(),
                     Math.sqrt(complexReal * complexReal + complexImag * complexImag));
        }

        MPInverseFourierTransform.completedTaskNum += 1;

        LocalTime nowTime = LocalTime.now();
        LocalTime tempTime = LocalTime.from(MPInverseFourierTransform.startTime);

        long elapsedHour = tempTime.until(nowTime, ChronoUnit.HOURS);
        tempTime = tempTime.plusHours(elapsedHour);

        long elapsedMin = tempTime.until(nowTime, ChronoUnit.MINUTES);
        tempTime = tempTime.plusMinutes(elapsedMin);

        long elapsedSec = tempTime.until(nowTime,ChronoUnit.SECONDS);
        tempTime = tempTime.plusSeconds(elapsedSec);

        System.out.println("Inverse Fourier tasks completed: " +
                MPInverseFourierTransform.completedTaskNum + "/" + MPInverseFourierTransform.taskNum +
                " --> " + String.format("%.2f",((double) MPInverseFourierTransform.completedTaskNum / (double) MPInverseFourierTransform.taskNum) * 100) +
                "% " + "Elapsed time: " + elapsedHour + " hour " + elapsedMin + " minutes " +
                elapsedSec + " seconds");
    }

    public ArrayList<MPPixel> getPixelPack() {
        return pixelPack;
    }

    public int getPixelPackSize() {
        return pixelPack.size();
    }

    public void addPixelToPack(MPPixel pixel) {
        pixelPack.add(pixel);
    }
}