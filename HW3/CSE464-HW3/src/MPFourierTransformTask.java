import vpt.Image;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class MPFourierTransformTask implements Runnable {
    private Image img,
                  fourierSpectrum,
                  fourierReal,
                  fourierImage;
    private ArrayList<MPPixel> pixelPack;

    public MPFourierTransformTask(Image img,
                                  Image fourierReal,
                                  Image fourierImage,
                                  Image fourierSpectrum) {
        pixelPack = new ArrayList<>();
        this.img = img;
        this.fourierReal = fourierReal;
        this.fourierImage = fourierImage;
        this.fourierSpectrum = fourierSpectrum;
    }

    @Override
    public void run() {
        int xDim = img.getXDim();
        int yDim = img.getYDim();

        for (MPPixel pixel: pixelPack) {
            double complexReal = 0.0;
            double complexImag = 0.0;

            int u = 0;
            int v = 0;

            while (u < xDim) {
                double res = ((double)u * (double)pixel.getX() / (double)xDim +
                        (double)v * (double)pixel.getY() / (double)yDim) * (Math.PI * -2);
                double pixelVal = img.getXYDouble(u,v);
                complexReal += pixelVal * Math.cos(res);
                complexImag += pixelVal * Math.sin(res);

                ++v;

                if (v >= yDim) {
                    v = 0;
                    ++u;
                }
            }

            complexReal = complexReal / Math.sqrt(xDim * yDim);
            complexImag = complexImag / Math.sqrt(xDim * yDim);

            fourierReal.setXYDouble(pixel.getX(),pixel.getY(), complexReal);
            fourierImage.setXYDouble(pixel.getX(),pixel.getY(), complexImag);
            fourierSpectrum.setXYDouble(pixel.getX(),pixel.getY(),
                    Math.sqrt(complexReal * complexReal + complexImag * complexImag));
        }

        MPFourierTransform.completedTaskNum += 1;

        LocalTime nowTime = LocalTime.now();
        LocalTime tempTime = LocalTime.from(MPFourierTransform.startTime);

        long elapsedHour = tempTime.until(nowTime, ChronoUnit.HOURS);
        tempTime = tempTime.plusHours(elapsedHour);

        long elapsedMin = tempTime.until(nowTime, ChronoUnit.MINUTES);
        tempTime = tempTime.plusMinutes(elapsedMin);

        long elapsedSec = tempTime.until(nowTime,ChronoUnit.SECONDS);
        tempTime = tempTime.plusSeconds(elapsedSec);

        System.out.println("Fourier tasks completed: " +
                MPFourierTransform.completedTaskNum + "/" + MPFourierTransform.taskNum +
                " --> " + String.format("%.2f",((double) MPFourierTransform.completedTaskNum / (double) MPFourierTransform.taskNum) * 100) +
                "% " + "Elapsed time: " + elapsedHour + " hour " + elapsedMin + " minutes " +
                elapsedSec + " seconds");
    }

    public ArrayList<MPPixel> getPixelPack() {
        return pixelPack;
    }

    public int getPixelPackSize() {
        return pixelPack.size();
    }

    public void addPixelToPack(MPPixel pixel) {
        pixelPack.add(pixel);
    }
}