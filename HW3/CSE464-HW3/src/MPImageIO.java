import vpt.DoubleImage;
import vpt.Image;

import java.io.*;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by mpolatcan-gyte_cse on 16.04.2017.
 */
// Utility functions for save double images as .txt files. (Written for save
// fourier transform's real and imaginary images).
public class MPImageIO {
    public static void saveImage(Image img, String path) {
        try {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path)));

            for (int i = 0; i < img.getYDim(); i++) {
                for (int j = 0; j < img.getXDim(); j++) {
                    writer.write("" + img.getXYDouble(j,i) + " ");
                }
                writer.write("\n");
            }

            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static DoubleImage loadImage(String path) {
        ArrayList<ArrayList<Double>> pixels = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(path)));

            String line = reader.readLine();

            int x = 0;
            int y = 0;

            while (line != null) {
                StringTokenizer tokenizer = new StringTokenizer(line);
                pixels.add(new ArrayList<>());
                while (tokenizer.hasMoreTokens()) {
                    pixels.get(y).add(x,Double.parseDouble(tokenizer.nextToken()));
                    ++x;
                }
                ++y;
                x = 0;
                line = reader.readLine();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        DoubleImage image = new DoubleImage(pixels.get(0).size(),pixels.size());
        for (int i = 0; i < image.getYDim(); i++) {
            for (int j = 0; j < image.getXDim(); j++) {
                image.setXYDouble(j,i,pixels.get(i).get(j));
            }
        }

        return image;
    }
}
