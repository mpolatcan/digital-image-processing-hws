import vpt.ByteImage;
import vpt.DoubleImage;
import vpt.Image;
import vpt.util.Tools;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by mpolatcan-gyte_cse on 30.03.2017.
 */
public class MPFourierTransform {
    public static LocalTime startTime;
    public static final int REAL_OUTPUT = 0;
    public static final int IMG_OUTPUT = 1;
    public static final int MAG_OUTPUT = 2;
    public static int taskNum;
    public static int completedTaskNum = 0;

    public static Image[] execute(Image img) {
        startTime = LocalTime.now();

        Image[] fourierResults = new DoubleImage[3];
        DoubleImage fourierReal = new DoubleImage(img.getXDim(),img.getYDim());
        DoubleImage fourierImage = new DoubleImage(img.getXDim(),img.getYDim());
        DoubleImage fourierSpectrum = new DoubleImage(img.getXDim(),img.getYDim());
        ArrayList<MPFourierTransformTask> fourierTasks = new ArrayList<>();
        ExecutorService fourierThreadPool = Executors.newFixedThreadPool(8);

        for (int i = 0; i < ((img.getXDim() * img.getYDim()) / 5) + 1; i++) {
            fourierTasks.add(new MPFourierTransformTask(img,fourierReal,fourierImage,fourierSpectrum));
        }

        taskNum = fourierTasks.size();

        System.out.println("Task num: " + taskNum);

        int currentTask =  0;

        for (int i = 0; i < img.getXDim(); i++) {
            for (int j = 0; j < img.getYDim(); j++) {
                if (fourierTasks.get(currentTask).getPixelPackSize() == 5) {
                    ++currentTask;
                }
                fourierTasks.get(currentTask).addPixelToPack(new MPPixel(i,j));
            }
        }

        for (MPFourierTransformTask task: fourierTasks) {
            fourierThreadPool.execute(task);
        }

        fourierThreadPool.shutdown();

        while (!fourierThreadPool.isTerminated());

        fourierResults[REAL_OUTPUT] = fourierReal;
        fourierResults[IMG_OUTPUT] = fourierImage;
        fourierResults[MAG_OUTPUT] = Tools.shiftOrigin(fourierSpectrum);

        return fourierResults;
    }
}
