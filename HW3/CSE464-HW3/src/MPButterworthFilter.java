import vpt.DoubleImage;
import vpt.Image;

/**
 * Created by mpolatcan-gyte_cse on 16.04.2017.
 */
public class MPButterworthFilter {
    public static Image getButterWorthFilter(int xDim, int yDim, int n, double innerRadius, int outerRadius) {
        Image butterworthFilter = new DoubleImage(xDim,yDim);

        int centerX = butterworthFilter.getXDim()/2;
        int centerY = butterworthFilter.getYDim()/2;

        for (int i = 0; i < butterworthFilter.getXDim(); i++) {
            for (int j = 0; j < butterworthFilter.getYDim(); j++) {
                double distance = Math.sqrt(Math.pow(i-centerX,2)+Math.pow(j-centerY,2));
                butterworthFilter.setXYDouble(i,j,butterworthFilterHelper(distance,innerRadius,outerRadius,n));
            }
        }

        return butterworthFilter;
    }

    public static double butterworthFilterHelper(double distance, double innerRadius, double outerRadius, int n) {
        return (1 / (1 + Math.pow((distance * outerRadius) / (Math.pow(distance,2) - innerRadius*innerRadius),(double)(2 * n))));
    }
}
