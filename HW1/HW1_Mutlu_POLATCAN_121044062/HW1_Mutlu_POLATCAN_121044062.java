import vpt.ByteImage;
import vpt.Image;
import vpt.IntegerImage;
import vpt.algorithms.display.Display2D;
import vpt.algorithms.io.Load;

import java.util.ArrayList;

/**
 * Created by mpolatcan-gyte_cse on 25.02.2017.
 */
public class HW1_Mutlu_POLATCAN_121044062 {
    public static void main(String[] args) {
        Image img = Load.invoke("waalsdorp_2.png");

        Display2D.invoke(scale(img,0.5,0.5,0), "Nearest Neighbor %50");
        Display2D.invoke(scale(img,0.5,0.5,1), "Bilinear Interpolation %50");
        Display2D.invoke(scale(img,2,2,0),"Nearest Neighbor %200");
        Display2D.invoke(scale(img,2,2,1),"Bilinear Interpolation %200");
        Display2D.invoke(equalize(img), "Equalized Normal");
        Display2D.invoke(equalizeAdaptively(img), "Equalized Adaptively");
        Display2D.invoke(equalizeAdaptivelyImproved(img), "Equalize Adaptively Improved");
    }

    /* --------------------- PART 1 -----------------------------------------*/
    public static Image scale(Image img, double sx, double sy, int strat) {
        switch (strat) {
            case 0:
                return nearestNeighborInterpolation(img,sx,sy);

            case 1:
                return bilinearInterpolation(img,sx,sy);

            case 2:
                return bicubicInterpolation(img,sx,sy);

            default:
                return null;
        }
    }

    private static Image nearestNeighborInterpolation(Image img, double sx, double sy) {
        Image result = new ByteImage(((int) (img.getXDim() * sx)), ((int) (img.getYDim() * sy)), 1);

        for (int i = 0; i < result.getXDim(); ++i) {
            for (int j = 0; j < result.getYDim(); ++j) {
               int x = ((int) Math.floor(i / sx));
               int y = ((int) Math.floor(j / sy));
               result.setXYByte(i,j,img.getXYByte(x,y));
            }
        }

        return result;
    }

    private static Image bilinearInterpolation(Image img, double sx, double sy) {
        Image result = new ByteImage(((int) (img.getXDim() * sx)), ((int) (img.getYDim() * sy)), 1);

        int newWidth = ((int) (img.getXDim() * sx));
        int newHeight = ((int) (img.getYDim() * sy));

        for (int y = 0; y < newHeight; y++) {
           for (int x = 0; x < newWidth; ++x) {
               float gx = x / (float)result.getXDim() * (img.getXDim() - 1);
               float gy = y / (float)result.getYDim() * (img.getYDim() - 1);
               int gxi = ((int) gx);
               int gyi = ((int) gy);

               int pixelResult = 0;

               int x00 = img.getXYByte(gxi,gyi);
               int x10 = img.getXYByte(gxi+1,gyi);
               int x01 = img.getXYByte(gxi,gyi+1);
               int x11 = img.getXYByte(gxi+1,gyi+1);

               for (int i = 0; i < 3; i++) {
                   pixelResult |= ((int) bilinearInterpolator(getByteVal(x00, i), getByteVal(x10, i), getByteVal(x01, i), getByteVal(x11, i), gx - gxi, gy - gyi)) << (i*8);
               }

               result.setXYByte(x,y,pixelResult);
           }
        }

        return result;
    }

    private static float getByteVal(int value, int n) {
        return value >> (n * 8) & 0xFF;
    }

    private static float linearInterpolator(float s, float e, float t) { return s + (e-s) * t; }

    private static float bilinearInterpolator(float x00, float x10, float x01, float x11,
                                        float tx, float ty) {
        return linearInterpolator(linearInterpolator(x00,x10,tx),
                                  linearInterpolator(x01,x11,tx),
                                  ty);
    }

    private static Image bicubicInterpolation(Image img, double sx, double sy) {
        Image result = new IntegerImage(((int) (img.getXDim() * sx)), ((int) (img.getYDim() * sy)));

        for (int i = 0; i < result.getXDim(); i++) {
            for (int j = 0; j < result.getYDim(); j++) {

            }
        }

        return result;
    }
    /* -------------------------------------------------------------------------*/

    /* --------------------------- PART 2 --------------------------------------*/
    public static Image equalize(Image img) {
        Image equalizedImg = img.newInstance(true);
        int[] imgHistogram = new int[256];
        double[] imgCumulativeHistogram = new double[256];

        // Calculate histogram of image (Compute PDF)
        for (int i = 0; i < img.getXDim(); ++i) {
            for (int j = 0; j < img.getYDim(); ++j) {
                ++imgHistogram[img.getXYByte(i, j)];
            }
        }

        // Calculate cumulative histogram of image
        imgCumulativeHistogram[0] = imgHistogram[0];
        for (int i = 1; i < 256; i++) {
            imgCumulativeHistogram[i] = imgHistogram[i] + imgCumulativeHistogram[i-1];
        }

        // Calculate CDFs
        for (int i = 0; i < 256; i++) {
            imgCumulativeHistogram[i] = imgCumulativeHistogram[i] / (img.getXDim() * img.getYDim());
        }

        // Map pixels according to CDFs
        for (int i = 0; i < img.getXDim(); i++) {
            for (int j = 0; j < img.getYDim(); j++) {
                equalizedImg.setXYByte(i,j, ((int) (255 * imgCumulativeHistogram[img.getXYByte(i, j)])));
            }
        }

        return equalizedImg;
    }
    /* ------------------------------------------------------------------------*/

    /* ----------------------- PART 3 -----------------------------------------*/
    public static Image equalizeAdaptively(Image img) {
        Image equalizedImg = img.newInstance(true);
        ArrayList<ArrayList<PixelCoor>> imageTiles = new ArrayList<>();
        ArrayList<int[]> tilesHistograms = new ArrayList<>();
        ArrayList<double[]> tilesCumulativeHistograms = new ArrayList<>();

        int tileNumber = 64;
        int currentTile;

        // Creating buckets for 80x6    0 image tiles and tiles histograms
        for (int i = 0; i < tileNumber; i++) {
            imageTiles.add(new ArrayList<>());
            tilesHistograms.add(new int[256]);
            tilesCumulativeHistograms.add(new double[256]);
        }

        for (int i = 0; i < img.getXDim(); i++) {
            for (int j = 0; j < img.getYDim(); ++j) {
                currentTile = (i / 80) + (8 * (j / 60));
                imageTiles.get(currentTile).add(new PixelCoor(i,j));
            }
        }

        // Calculate local histogram for every tile (Compute PDF)
        for (int i = 0; i < tileNumber; i++) {
            int[] localHistogram = tilesHistograms.get(i);
            ArrayList<PixelCoor> imageTile = imageTiles.get(i);

            for (PixelCoor coor : imageTile) {
                localHistogram[img.getXYByte(coor.x,coor.y)]++;
            }
        }

        // Calculate local cumulative histogram for every
        for (int i = 0; i < tileNumber; i++) {
            int[] localHistogram = tilesHistograms.get(i);
            double[] localCumulativeHistogram = tilesCumulativeHistograms.get(i);

            localCumulativeHistogram[0] = (double) localHistogram[0];
            for (int j = 1; j < 256; ++j) {
                localCumulativeHistogram[j] = localCumulativeHistogram[j-1] + localHistogram[j];
            }
        }

        // Calculate CDFs for every tile
        for (int i = 0; i < tileNumber; i++) {
            double[] localCumulativeHistogram = tilesCumulativeHistograms.get(i);

            for (int j = 0; j < 256; ++j) {
                localCumulativeHistogram[j] = localCumulativeHistogram[j] / 4800;
            }
        }

        // Map pixels according to CDFs for every tile
        for (int i = 0; i < tileNumber; i++) {
            double[] localCumulativeHistogram = tilesCumulativeHistograms.get(i);

            for (PixelCoor coor : imageTiles.get(i)) {
                equalizedImg.setXYByte(coor.x,coor.y,
                    ((int) Math.round(255 * localCumulativeHistogram[img.getXYByte(coor.x, coor.y)])));
            }
        }

        return equalizedImg;
    }
    /* -----------------------------------------------------------------------*/

    /* ------------------------------ PART 4 ---------------------------------*/
    public static Image equalizeAdaptivelyImproved(Image img) {
        Image equalizedImg = img.newInstance(true);
        ArrayList<ArrayList<PixelCoor>> imageTiles = new ArrayList<>();
        ArrayList<int[]> tilesHistograms = new ArrayList<>();
        ArrayList<double[]> tilesCumulativeHistograms = new ArrayList<>();

        int tileNumber = 64;
        int currentTile;

        // Creating buckets for 80x60 image tiles and tiles histograms
        for (int i = 0; i < tileNumber; i++) {
            imageTiles.add(new ArrayList<>());
            tilesHistograms.add(new int[256]);
            tilesCumulativeHistograms.add(new double[256]);
        }

        for (int i = 0; i < img.getXDim(); i++) {
            for (int j = 0; j < img.getYDim(); ++j) {
                currentTile = i / 80 + (8 * (j / 60));
                imageTiles.get(currentTile).add(new PixelCoor(i,j));
            }
        }

        // Calculate local histogram for every tile (Compute PDF)
        for (int i = 0; i < tileNumber; i++) {
            int[] localHistogram = tilesHistograms.get(i);
            ArrayList<PixelCoor> imageTile = imageTiles.get(i);

            for (PixelCoor coor : imageTile) {
                localHistogram[img.getXYByte(coor.x,coor.y)]++;
            }

            if ((tileNumber + 1 % 8 != 0) && (i < 54)) {
                ArrayList<PixelCoor> imageTileEast = imageTiles.get(i + 1);
                ArrayList<PixelCoor> imageTileSouth = imageTiles.get(i + 8);
                ArrayList<PixelCoor> imageTileSouthEast = imageTiles.get(i + 9);

                for (PixelCoor coor : imageTileEast) {
                    localHistogram[img.getXYByte(coor.x,coor.y)]++;
                }

                for (PixelCoor coor : imageTileSouth) {
                    localHistogram[img.getXYByte(coor.x,coor.y)]++;
                }

                for (PixelCoor coor : imageTileSouthEast) {
                    localHistogram[img.getXYByte(coor.x,coor.y)]++;
                }
            }
        }

        // Calculate local cumulative histogram for every
        for (int i = 0; i < tileNumber; i++) {
            int[] localHistogram = tilesHistograms.get(i);
            double[] localCumulativeHistogram = tilesCumulativeHistograms.get(i);

            localCumulativeHistogram[0] = (double) localHistogram[0];
            for (int j = 1; j < 256; ++j) {
                localCumulativeHistogram[j] = localCumulativeHistogram[j-1] + localHistogram[j];
            }
        }

        // Calculate CDFs for every tile
        for (int i = 0; i < tileNumber; i++) {
            double[] localCumulativeHistogram = tilesCumulativeHistograms.get(i);

            for (int j = 0; j < 256; ++j) {
                if ((tileNumber + 1 % 8 != 0) && (i < 54)) {
                    localCumulativeHistogram[j] = localCumulativeHistogram[j] / 19200;
                } else {
                    localCumulativeHistogram[j] = localCumulativeHistogram[j] / 4800;
                }
            }
        }

        // Map pixels according to CDFs for every tile
        for (int i = 0; i < tileNumber; i++) {
            double[] localCumulativeHistogram = tilesCumulativeHistograms.get(i);
            for (PixelCoor coor : imageTiles.get(i)) {
                equalizedImg.setXYByte(coor.x, coor.y,
                        ((int) Math.round(255 * localCumulativeHistogram[img.getXYByte(coor.x, coor.y)])));
            }
        }

        return equalizedImg;
    }
    /* -----------------------------------------------------------------------*/

    public static class PixelCoor {
        int x;
        int y;

        public PixelCoor(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}
