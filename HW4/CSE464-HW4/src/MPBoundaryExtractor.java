import vpt.ByteImage;
import vpt.Image;

/**
 * Created by mpolatcan-gyte_cse on 20.05.2017.
 */
public class MPBoundaryExtractor {
    public static Image extractBoundaryImage(Image image) {
        Image boundaryImage = new ByteImage(image.getXDim(),image.getYDim());

        // Get y derivation of image
        for (int i = 1; i < image.getYDim() - 1; i++) {
            for (int j = 0; j < image.getXDim(); j++) {
                if (image.getXYByte(j,i-1) == 0 && checkIntensity(image.getXYByte(j,i)) ||
                    image.getXYByte(j,i + 1) == 0 && checkIntensity(image.getXYByte(j,i))) {
                    boundaryImage.setXYByte(j,i,255);
                } else {
                    boundaryImage.setXYByte(j,i,0);
                }
            }
        }

        // Get x derivation of image
        for (int i = 1; i < image.getXDim() - 1; i++) {
            for (int j = 0; j < image.getYDim(); j++) {
               if (boundaryImage.getXYByte(i,j) == 0) {
                   if (image.getXYByte(i-1,j) == 0 && checkIntensity(image.getXYByte(i,j)) ||
                       image.getXYByte(i+1,j) == 0 && checkIntensity(image.getXYByte(i,j))) {
                       boundaryImage.setXYByte(i,j,255);
                   }
               }
            }
        }

        return boundaryImage;
    }

    private static boolean checkIntensity(int intensity) {
        if (intensity >= 150) {
            return true;
        } else {
            return false;
        }
    }
}
