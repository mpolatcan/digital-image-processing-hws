import vpt.Image;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 * Created by mpolatcan-gyte_cse on 19.05.2017.
 */
public class MPFourierDescriptor {
    private MPComplex[] coefficients;
    private String className;

    public MPFourierDescriptor(MPComplex[] coefficients, String className) {
        this.coefficients = coefficients;
        this.className = className;
    }

    public MPFourierDescriptor(Image image, String className) {
        this.className = className;
        calculateFourierDescriptor(image,className);
    }

    public String getClassName() {
        return className;
    }

    public MPComplex[] getCoefficients() {
        return coefficients;
    }

    private void calculateFourierDescriptor(Image image, String imageName) {
        ArrayList<MPPixel> boundaryPixels = getOrderedBoundaryPixels(image);
        MPComplex[] boundaryPixelsComplexNumbers = new MPComplex[boundaryPixels.size()];

        /* Convert boundary pixels to complex number form */
        for (int i = 0; i < boundaryPixels.size(); i++) {
            MPPixel currentPixel = boundaryPixels.get(i);
            boundaryPixelsComplexNumbers[i] = new MPComplex(currentPixel.getX(), currentPixel.getY());
        }

        coefficients = oneDimensionalDFT(boundaryPixelsComplexNumbers);

        if (imageName != null) {
            // Save training descriptors...
            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter("descriptors/" + imageName + ".txt"));

                for (int i = 0; i < coefficients.length; i++) {
                    writer.write(coefficients[i].toString());
                    writer.write("\n");
                }

                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private ArrayList<MPPixel> getOrderedBoundaryPixels(Image image) {
        ArrayList<MPPixel> boundaryPixels = getBoundaryPixels(image);
        ArrayList<MPPixel> orderedBoundaryPixels = new ArrayList<>();
        int boundaryPixelsSize = boundaryPixels.size();

        try {
            orderedBoundaryPixels.add(boundaryPixels.get(0)); // add initial pixel (starting point)
        } catch (Exception ex) {
            System.out.println(className);
        }

        boundaryPixels.remove(0); // remove it to prevent adding duplicate pixel

        while (orderedBoundaryPixels.size() < boundaryPixelsSize) {
            int nearestPixelIndex = 0;
            double nearestPixelDistance = Double.MAX_VALUE;

            for (int i = 0; i < boundaryPixels.size(); i++) {
                double distance = MPDistanceMetrics.euclideanDistancePixel(
                        orderedBoundaryPixels.get(orderedBoundaryPixels.size()-1),boundaryPixels.get(i));
                if (distance < nearestPixelDistance) {
                    nearestPixelDistance = distance;
                    nearestPixelIndex = i;
                }
            }

            orderedBoundaryPixels.add(boundaryPixels.get(nearestPixelIndex));
            boundaryPixels.remove(nearestPixelIndex);
        }

        return orderedBoundaryPixels;
    }


    private ArrayList<MPPixel> getBoundaryPixels(Image image) {
        ArrayList<MPPixel> boundaryPixels = new ArrayList<>();

        for (int i = 0; i < image.getXDim(); i++) {
            for (int j = 0; j < image.getYDim(); j++) {
                if (image.getXYByte(i,j) == 255 || image.getXYByte(i,j) == 250) {
                    boundaryPixels.add(new MPPixel(i, j));
                }
            }
        }

        return boundaryPixels;
    }


    private MPComplex[] oneDimensionalDFT(MPComplex[] boundaryPixelsComplexNumbers) {
        int coefficientsSize = boundaryPixelsComplexNumbers.length;
        MPComplex[] coefficients = new MPComplex[coefficientsSize];

        for (int i = 0; i < coefficientsSize; i++) {
            double sumReal = 0;
            double sumImag = 0;

            for (int j = 0; j < coefficientsSize; j++) {
                double res = (-2 * Math.PI * i * j) / coefficientsSize;
                MPComplex currentPixel = boundaryPixelsComplexNumbers[j];
                sumReal += currentPixel.getReal() * Math.cos(res) + currentPixel.getImag() * Math.sin(res);
                sumImag += -currentPixel.getReal() * Math.sin(res) + currentPixel.getImag() * Math.cos(res);
            }

            coefficients[i] = new MPComplex(sumReal/Math.sqrt(coefficientsSize),
                                            sumImag/Math.sqrt(coefficientsSize));
        }

        return coefficients;
    }

    public MPComplex[] oneDimensionalIDFT(MPComplex[] coefficients) {
        int boundaryPixelsSize = coefficients.length;
        System.out.println("Coefficients size: " + boundaryPixelsSize);
        MPComplex[] boundaryPixelsComplexNumbers = new MPComplex[boundaryPixelsSize];

        for (int i = 0; i < boundaryPixelsSize; i++) {
            double sumReal = 0;
            double sumImag = 0;

            for (int j = 0; j < boundaryPixelsSize; j++) {
                double res = (2 * Math.PI * i * j) / boundaryPixelsSize;
                MPComplex currentCoefficient = coefficients[j];

                sumReal += currentCoefficient.getReal() * Math.cos(res) - currentCoefficient.getImag() * Math.sin(res);
                sumImag += currentCoefficient.getReal() * Math.sin(res) + currentCoefficient.getImag() * Math.cos(res);
            }

            boundaryPixelsComplexNumbers[i] = new MPComplex(sumReal/Math.sqrt(boundaryPixelsSize),
                                                            sumImag/Math.sqrt(boundaryPixelsSize));
        }

        return boundaryPixelsComplexNumbers;
    }
}
