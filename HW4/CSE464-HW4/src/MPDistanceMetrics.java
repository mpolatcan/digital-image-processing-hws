/**
 * Created by mpolatcan-gyte_cse on 21.05.2017.
 */
public class MPDistanceMetrics {
    public static double euclideanDistanceDescriptor(MPComplex[] descriptor, MPComplex[] otherDescriptor,
                                                     int coefficientsSize) {
        double total = 0;

        if (coefficientsSize > otherDescriptor.length)
            coefficientsSize = otherDescriptor.length;

        for (int i = 0; i < coefficientsSize; i++) {
            MPComplex diff = new MPComplex(descriptor[i].getReal()-otherDescriptor[i].getReal(),
                                           descriptor[i].getImag()-otherDescriptor[i].getImag());
            total += (Math.pow(diff.getReal(),2) + Math.pow(diff.getImag(),2));
        }

        return Math.sqrt(total);
    }

    public static double manhattanDistanceDescriptor(MPComplex[] descriptor, MPComplex[] otherDescriptor,
                                                     int coefficientsSize) {
        double total = 0;

        if (coefficientsSize > otherDescriptor.length)
            coefficientsSize = otherDescriptor.length;

        for (int i = 0; i < coefficientsSize; i++) {
            MPComplex diff = new MPComplex(descriptor[i].getReal()-otherDescriptor[i].getReal(),
                    descriptor[i].getImag()-otherDescriptor[i].getImag());
            total += Math.sqrt(Math.pow(diff.getReal(),2) + Math.pow(diff.getImag(),2)) ;
        }

        return total;
    }

    public static double chiSquareDistanceDescriptor(MPComplex[] descriptor, MPComplex[] otherDescriptor,
                                                     int coefficientsSize) {
        double total = 0;

        if (coefficientsSize > otherDescriptor.length)
            coefficientsSize = otherDescriptor.length;

        for (int i = 0; i < coefficientsSize; i++) {
            MPComplex diff = new MPComplex(descriptor[i].getReal()-otherDescriptor[i].getReal(),
                    descriptor[i].getImag()-otherDescriptor[i].getImag());
            MPComplex sum = new MPComplex(descriptor[i].getReal()+otherDescriptor[i].getReal(),
                    descriptor[i].getImag()+otherDescriptor[i].getImag());

            total += (Math.pow(diff.getReal(),2) + Math.pow(diff.getImag(),2)) /
                     Math.sqrt((Math.pow(sum.getReal(),2) + Math.pow(sum.getImag(),2)));
        }

        return total;
    }

    public static double euclideanDistancePixel(MPPixel pixel, MPPixel otherPixel) {
        return Math.sqrt(Math.pow(pixel.getX() - otherPixel.getX(),2) + Math.pow(pixel.getY() - otherPixel.getY(),2));
    }
}
