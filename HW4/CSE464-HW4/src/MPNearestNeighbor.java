import vpt.Image;
import vpt.algorithms.io.Load;
import vpt.algorithms.io.Save;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by mpolatcan-gyte_cse on 19.05.2017.
 */
public class MPNearestNeighbor {
    private static int DATA_SIZE = 699;
    private static String DESCRIPTORS_PATH = "descriptors";
    private static String BOUNDARIRES_PATH = "boundaries";
    private static String TRAIN_DATA_PATH = "mpeg7shapeB/train";
    private static int EUCLIDEAN = 1;
    private static int MANHATTAN = 2;
    private static int CHI_SQUARE = 3;
    private ArrayList<MPFourierDescriptor> descriptors;
    private double correctPrediction = 0;
    private double completedPredictions = 0;

    public MPNearestNeighbor() {
        descriptors = new ArrayList<>();
        prepareTrainData();
    }

    public String execute(Image image, int coefficientsSize, int distanceMetric) {
        Image boundaryImage = MPBoundaryExtractor.extractBoundaryImage(image);
        MPFourierDescriptor testImageDescriptor = new MPFourierDescriptor(boundaryImage,null);

        if (coefficientsSize > testImageDescriptor.getCoefficients().length)
            coefficientsSize = testImageDescriptor.getCoefficients().length;

        return findNearestNeighbor(testImageDescriptor, coefficientsSize, distanceMetric);
    }

    public void incrementCorrectPrediction() {
        ++correctPrediction;
    }

    public void incrementCompletedPrediction() { ++completedPredictions; }

    public double getCompletedPredictions() {
        return completedPredictions;
    }

    public double getCorrectPrediction() {
        return correctPrediction;
    }

    private String findNearestNeighbor(MPFourierDescriptor testImageDescriptor, int coefficientsSize,
                                       int distanceMetric) {
        MPFourierDescriptor nearestNeighborDescriptor = null;
        double minDistance = Double.MAX_VALUE;

        for (MPFourierDescriptor trainImageDescriptor : descriptors) {
            double distance = 0;
            if (distanceMetric == EUCLIDEAN) {
                distance = MPDistanceMetrics.euclideanDistanceDescriptor(
                        testImageDescriptor.getCoefficients(), trainImageDescriptor.getCoefficients(), coefficientsSize);
            } else if (distanceMetric == MANHATTAN) {
                distance = MPDistanceMetrics.manhattanDistanceDescriptor(
                        testImageDescriptor.getCoefficients(), trainImageDescriptor.getCoefficients(), coefficientsSize);
            } else if (distanceMetric == CHI_SQUARE) {
                distance = MPDistanceMetrics.chiSquareDistanceDescriptor(
                        testImageDescriptor.getCoefficients(), trainImageDescriptor.getCoefficients(), coefficientsSize);
            }

            if (distance < minDistance) {
                nearestNeighborDescriptor = trainImageDescriptor;
                minDistance = distance;
            }
        }

        return nearestNeighborDescriptor.getClassName();
    }

    private void prepareTrainData() {
        File trainDataDirectory = new File(TRAIN_DATA_PATH);
        File boundariesDirectory = new File(BOUNDARIRES_PATH);
        File descriptorsDirectory = new File(DESCRIPTORS_PATH);

        int boundariesDirectorySize = boundariesDirectory.listFiles().length;
        int descriptorsDirectorySize = descriptorsDirectory.listFiles().length;

        if (boundariesDirectorySize != DATA_SIZE) {
            double size = trainDataDirectory.listFiles().length;
            double completedSize = 0;

            // Prepare boundaries of images
            for (File image : trainDataDirectory.listFiles()) {
                System.out.println("Boundary of " + image.getName() + " preparing... " +
                        String.format("%.2f", 100 * (completedSize / size)) + "%");
                Image trainImage = Load.invoke(image.getPath());
                Image boundaryImage = MPBoundaryExtractor.extractBoundaryImage(trainImage);
                Save.invoke(boundaryImage, "boundaries/" + image.getName());
                ++completedSize;
            }
        } else {
            System.out.println("Boundaries have already prepared...");
        }

        if (descriptorsDirectorySize != DATA_SIZE) {
            double size = boundariesDirectory.listFiles().length;
            double completedSize = 0;

            // Prepare descriptors of images
            for (File image : boundariesDirectory.listFiles()) {
                System.out.println("Descriptor of " + image.getName() + " preparing... " +
                        String.format("%.2f", 100 * (completedSize / size)) + "%");
                Image boundaryImage = Load.invoke(image.getPath());

                StringTokenizer tokenizer = new StringTokenizer(image.getName(), ".");
                descriptors.add(new MPFourierDescriptor(boundaryImage, tokenizer.nextToken()));
                ++completedSize;
            }
        } else {
            System.out.println("Descriptors have already prepared...");

            double size = descriptorsDirectory.listFiles().length;
            double completedSize = 0;

            // Load prepared descriptors....
            for (File descriptor : descriptorsDirectory.listFiles()) {
                System.out.println("Descriptor " + descriptor.getName() + " loading... " +
                        String.format("%.2f", 100 * (completedSize / size)) + "%");
                try {
                    BufferedReader reader = new BufferedReader(new FileReader(descriptor));
                    ArrayList<MPComplex> descriptorData = new ArrayList<>();
                    String line;

                    line = reader.readLine();

                    while (line != null) {
                        StringTokenizer tokenizer = new StringTokenizer(line);

                        while (tokenizer.hasMoreTokens()) {
                            double real = Double.parseDouble(tokenizer.nextToken());
                            double imag = Double.parseDouble(tokenizer.nextToken());
                            descriptorData.add(new MPComplex(real,imag));
                        }

                        line = reader.readLine();
                    }

                    reader.close();

                    MPComplex[] descriptorDataArray = new MPComplex[descriptorData.size()];

                    for (int i = 0; i < descriptorData.size(); i++) {
                        descriptorDataArray[i] = descriptorData.get(i);
                    }

                    StringTokenizer tokenizer = new StringTokenizer(descriptor.getName(),"-.");
                    descriptors.add(new MPFourierDescriptor(descriptorDataArray,tokenizer.nextToken()));
                    ++completedSize;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
