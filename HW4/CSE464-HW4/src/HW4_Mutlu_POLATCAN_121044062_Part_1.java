import vpt.DoubleImage;
import vpt.Image;
import vpt.algorithms.display.Display2D;
import vpt.algorithms.io.Load;

/**
 * Created by mpolatcan-gyte_cse on 04.05.2017.
 */
// Purple (128,0,128)
// Orange (255,165,0)
public class HW4_Mutlu_POLATCAN_121044062_Part_1 {
    public static void main(String[] args) {
        Image image = Load.invoke("question_1_kaplumbaga_terbiyecisi_osman_hamdi_bey.jpg");
        Image coloredImage = new DoubleImage(image.getXDim(),image.getYDim(),3);

        for (int i = 0; i < image.getXDim(); i++) {
            for (int j = 0; j < image.getYDim(); j++) {
                coloredImage.setXYCByte(i,j,0,redColorTransform(image.getXYByte(i,j)));
                coloredImage.setXYCByte(i,j,1,greenColorTransform(image.getXYByte(i,j)));
                coloredImage.setXYCByte(i,j,2,blueColorTransform(image.getXYByte(i,j)));
            }
        }

        Display2D.invoke(coloredImage,true);
    }

    public static int redColorTransform(int intensity) {
        int result;

        result = (intensity / 2) + 128;

        return result;
    }

    public static int greenColorTransform(int intensity) {
        int result;

        result = intensity / 2;

        return result;
    }

    public static int blueColorTransform(int intensity) {
        int result;

        result =  128 - intensity / 2;

        return result;
    }
}
