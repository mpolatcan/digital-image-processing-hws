import java.util.Locale;

/**
 * Created by mpolatcan-gyte_cse on 21.05.2017.
 */
public class MPComplex {
    private double real;
    private double imag;

    public MPComplex(double real, double imag) {
        this.real = real;
        this.imag = imag;
    }

    public double getImag() {
        return imag;
    }

    public double getReal() {
        return real;
    }

    @Override
    public String toString() {
        return String.format(Locale.US, "%f %f",real,imag);
    }
}
