import vpt.Image;
import vpt.algorithms.io.Load;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by mpolatcan-gyte_cse on 21.05.2017.
 */
public class HW4_Mutlu_POLATCAN_121044062_Part_2 {
    public static void main(String[] args) {
        File testDataDirectory = new File("mpeg7shapeB/test");
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter coefficient number: ");
        final int coefficientNum = scanner.nextInt();
        System.out.print("Distance metric: (1-Euclidean 2-Manhattan 3-Chi-Square): ");
        final int distanceMetric = scanner.nextInt();

        MPNearestNeighbor nearestNeighbor = new MPNearestNeighbor();
        ExecutorService nnThreadPool = Executors.newFixedThreadPool(8);
        ArrayList<Runnable> tasks = new ArrayList<>();

        for (File testImage : testDataDirectory.listFiles()) {
            tasks.add(new Runnable() {
                @Override
                public void run() {
                    StringTokenizer tokenizer = new StringTokenizer(testImage.getName(),".-");
                    String className = tokenizer.nextToken();

                    Image image = Load.invoke(testImage.getPath());
                    String predictedClassName = nearestNeighbor.execute(image, coefficientNum, distanceMetric);

                    nearestNeighbor.incrementCompletedPrediction();

                    System.out.println("File: " + testImage.getName() + " | Actual class type: " + className + " | Predicted class type: " + predictedClassName +
                        " | Completed: " + String.format("%.2f",100 * (nearestNeighbor.getCompletedPredictions() / tasks.size())) +
                        " | Accuracy: " + String.format("%.2f",100 * (nearestNeighbor.getCorrectPrediction()/testDataDirectory.listFiles().length)));

                    if (predictedClassName.equals(className)) {
                        nearestNeighbor.incrementCorrectPrediction();
                    }
                }
            });
        }

        for (Runnable nnTask : tasks) {
            nnThreadPool.execute(nnTask);
        }

        nnThreadPool.shutdown();

        while (!nnThreadPool.isTerminated());
    }
}
