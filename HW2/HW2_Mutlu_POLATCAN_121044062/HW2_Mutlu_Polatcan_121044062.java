import vpt.Image;
import vpt.algorithms.display.Display2D;
import vpt.algorithms.io.Load;

/**
 * Created by mpolatcan-gyte_cse on 12.03.2017.
 */
public class HW2_Mutlu_Polatcan_121044062 {
    public static Image cat;

    public static void main(String[] args) {
        cat = Load.invoke("perfectly_painted_cat.png");

        Display2D.invoke(cat, "Original");

        applyFirstMask(cat);
        applySecondMask(cat);
        applyThirdMask(cat);
        applyFourthMask(cat);

        Display2D.invoke(cat,"Result");
    }

    public static void applyFirstMask(Image img) {
        int[][] firstMask = {{255, 0, 0},
                             {255, 255, 0},
                             {255, 0, 0}};
        boolean modified;

        do {
            modified = false;

            for (int i = 0; i < img.getXDim(); i++) {
                for (int j = 0; j < img.getYDim(); j++) {
                    if (checkBounds(i - 1, j - 1) &&
                            checkBounds(i - 1, j) &&
                            checkBounds(i - 1, j + 1)) {
                        if (img.getXYByte(i - 1, j - 1) == firstMask[0][0] &&
                            img.getXYByte(i - 1, j) == firstMask[1][0] &&
                            img.getXYByte(i - 1, j + 1) == firstMask[2][0]) {
                            if (img.getXYByte(i,j) != firstMask[1][1]) {
                                img.setXYByte(i, j, firstMask[1][1]);
                                modified = true;
                            }
                        }
                    }
                }
            }
        } while(modified);
    }

    public static void applySecondMask(Image img) {
        int[][] secondMask = {{255, 255, 255},
                              {0, 255, 0},
                              {0, 0, 0}};
        boolean modified;

        do {
            modified = false;
            for (int i = 0; i < img.getXDim(); i++) {
                for (int j = 0; j < img.getYDim(); j++) {
                    if (checkBounds(i-1,j-1) &&
                        checkBounds(i,j-1) &&
                        checkBounds(i+1,j-1)) {
                        if (img.getXYByte(i - 1, j - 1) == secondMask[0][0] &&
                            img.getXYByte(i, j - 1) == secondMask[0][1] &&
                            img.getXYByte(i + 1, j - 1) == secondMask[0][2]) {
                            if (img.getXYByte(i, j) != secondMask[1][1]) {
                                img.setXYByte(i, j, secondMask[1][1]);
                                modified = true;
                            }
                        }
                    }
                }
            }
        } while (modified);
    }

    public static void applyThirdMask(Image img) {
        int[][] thirdMask = {{0, 0, 255},
                             {0, 255, 255},
                             {0, 0, 255}};
        boolean modified;

        do {
            modified = false;

            for (int i = 0; i < img.getXDim(); i++) {
                for (int j = 0; j < img.getYDim(); j++) {
                    if (checkBounds(i + 1, j - 1) &&
                        checkBounds(i + 1, j) &&
                        checkBounds(i + 1, j + 1)) {
                        if (img.getXYByte(i + 1, j - 1) == thirdMask[0][2] &&
                            img.getXYByte(i + 1, j) == thirdMask[1][2] &&
                            img.getXYByte(i + 1, j + 1) == thirdMask[2][2]) {
                            if (img.getXYByte(i,j) != thirdMask[1][1]) {
                                img.setXYByte(i, j, thirdMask[1][1]);
                                modified = true;
                            }
                        }
                    }
                }
            }
        } while(modified);
    }

    public static void applyFourthMask(Image img) {
        int[][] fourthMask = {{0, 0, 0},
                              {0, 255, 0},
                              {255, 255, 255}};
        boolean modified;

        do {
            modified = false;

            for (int i = 0; i < img.getXDim(); i++) {
                for (int j = 0; j < img.getYDim(); j++) {
                    if (checkBounds(i-1,j+1) &&
                        checkBounds(i,j+1) &&
                        checkBounds(i+1,j+1)) {
                        if (img.getXYByte(i - 1, j + 1) == fourthMask[2][0] &&
                            img.getXYByte(i, j + 1) == fourthMask[2][1] &&
                            img.getXYByte(i + 1, j + 1) == fourthMask[2][2]) {
                            if (img.getXYByte(i, j) != fourthMask[1][1]) {
                                img.setXYByte(i, j, fourthMask[1][1]);
                                modified = true;
                            }
                        }
                    }
                }
            }
        } while (modified);
    }

    private static boolean checkBounds(int x, int y) {
        return (x > 0 && x < cat.getXDim()) && (y > 0 && y < cat.getYDim());
    }
}
